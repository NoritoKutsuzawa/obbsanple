﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonEventModel : MonoBehaviour
{
	public void OnToNextButtonClicked()
	{
		Debug.Log("OnToNextButtonClicked");
		SceneManager.LoadScene("NextScene");
	}

	public void OnToTopButtonClicked()
	{
		Debug.Log("OnToTopButtonClicked");
		SceneManager.LoadScene("TopScene");
	}

	[SerializeField]
	private Image loadImage;

	public void OnResourceLoadButtonClicked()
	{
		Debug.Log("OnResourceLoadButtonClicked");
		var resource = Resources.Load<Texture2D>("icon_key");
		loadImage.sprite = Sprite.Create(resource,
									   new Rect(0, 0, resource.width, resource.height),
									   Vector2.zero);

	}

	[SerializeField] private Text loadResultText;

	public void OnStreamingAssetsButtonClicked()
	{
		Debug.Log("OnStreamingAssetsButtonClicked");

		var path = Path.Combine(Application.streamingAssetsPath, "load_sample.txt");
		//using (var reader = new StreamReader(path, Encoding.UTF8))
		//{
		//	var textData = reader.ReadLine();
		//	loadResultText.text = textData;
		//}

		//var readResult = File.ReadAllText(path);
		//loadResultText.text = readResult;

		var www = new WWW(path);
		while (!www.isDone)
		{
			
		}
		var readResult = www.text;
		loadResultText.text = readResult;

#if UNITY_ANDROID
				Debug.Log("UNITY_ANDROID");
	
#elif PLATFORM_ANDROID
	Debug.Log("PLATFORM_ANDROID");

#endif



	}
}
