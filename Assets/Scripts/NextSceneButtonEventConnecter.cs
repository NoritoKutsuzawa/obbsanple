﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextSceneButtonEventConnecter : ButtonEventConnecter
{
	public override void AddListenerToButton()
	{
		uiButton.onClick.AddListener(() => { buttonEventModel.OnToTopButtonClicked(); });
	}
}
