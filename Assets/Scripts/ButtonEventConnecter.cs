﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonEventConnecter : MonoBehaviour
{
	[SerializeField]
	protected ButtonEventModel buttonEventModel;

	[SerializeField]
	protected Button uiButton;

	// Use this for initialization
	void Start()
	{
		AddListenerToButton();
	}

	public virtual void AddListenerToButton()
	{
	}

	// Update is called once per frame
	void Update()
	{

	}
}
