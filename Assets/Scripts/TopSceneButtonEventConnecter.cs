﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopSceneButtonEventConnecter : ButtonEventConnecter
{
	[SerializeField]
	private Button resourceLoadButton;

	[SerializeField]
	private Button streamingAssetsLoadButton;

	public override void AddListenerToButton()
	{
		uiButton.onClick.AddListener(() => { buttonEventModel.OnToNextButtonClicked(); });
		resourceLoadButton.onClick.AddListener(() => { buttonEventModel.OnResourceLoadButtonClicked(); });
		streamingAssetsLoadButton.onClick.AddListener(() => { buttonEventModel.OnStreamingAssetsButtonClicked(); });
	}
}
